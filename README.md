Architecure:
1. Excel and Python for data pre-processing and EDA
2. MySQL, Node.js for the server side
3. React.js, CSS, JavaScript and HTML for the client side

Description:
There are three pages in the front end site:
1. Main Page: This page will include different types of searches such as title, direction,
language, country, duration, genre, date, published, title, year, description, or
actors. One can filter through the movies by title, director, year, duration, or ratings.
There is a drop down option for different genres. And a search box that allows
searches for different directors. You can also click on any movie listed and it will
take you to the Movies Page that allows you to view the movie in much more detail.
2. Movie Page: This page displays movie ranking based on gender and age from user
input. Users can choose their gender and which age group they belong to and the
most highly rated movies rated by this group will be displayed. The page also
displays movie details such as title, duration, country, language, ratings, genre,
director, actors, description in addition to movie posters with an “Like it” option.
Once a user clicks this button, the app will add this movie to a list and the
recommendation page will recommend based on this list.
3. Recommendation Page: This page recommends movies for the user based on
movies they’ve liked by searching for other movies with the same directors as the
movies they’ve liked. When a user likes a movie on the “Movie Page”, that movie is
added to a table called “Watchlist” within the database. This database is then used
via a series of joins to return movie posters of movies with the same directors as
those in the “Watchlist” table.

Data was pulled from 3 different pages:
1. Movies - 22 columns, 85,855 rows, 47.58 MB
https://www.kaggle.com/mirajshah07/netflix-dataset?select=IMDb+movies.csv
2. Ratings - 49 columns, 85,855 rows, 14.29MB
https://www.kaggle.com/mirajshah07/netflix-dataset?select=IMDb+ratings.csv
3. Posters - 19 columns, 40,000+ rows, 9.04 MB
https://www.kaggle.com/dadajonjurakuziev/movieposter?select=duplicate_free_41K.csv

Only movies published after 2000 were selected. Columns that don't give much insight
into the analysis were dropped. Rows that have any nulls in them were dropped. This
reduced the number of records from 85,855 to 47,138 in both Movies and Ratings
tables. In the Movie table, multiple data types were converted, e.g. convert ‘year’ to
integer and ‘date_published’ to datetime. All three tables share a common key
‘imdb_id’ which we will use to JOIN tables in our queries.
